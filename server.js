gpUsers = []
gruppen = []

let zeitstempel
let mentimeterUrl = ""
let mentimeterKey = ""
let titel = ""
let maxProGruppe
let dauerNachdenken
let endeUhrzeitNachdenken
let dauerVergleichen
let endeUhrzeitVergleichen
let dauerKonsens
let endeUhrzeitKonsens
let urls = ""
let themen = ""

const env = process.env.NODE_ENV || 'development';
const config = require('./config')[env];
const { GpUser } = require("./GpUser");
const { Gruppe } = require("./Gruppe");
const express = require('express')
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

const mysql = require('mysql')
const dbVerbindung = mysql.createConnection({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.db
})


dbVerbindung.connect()

dbVerbindung.query("INSERT INTO gruppen(laufendeNr, name) VALUES ('1','hallo';", (err, result) => {                                 
})

dbVerbindung.query("SHOW TABLES LIKE 'gruppenpuzzle';", (rows) => {

    console.log(rows)

    if (!rows){
        dbVerbindung.query("CREATE TABLE IF NOT EXISTS gruppenpuzzle(nummer INT AUTO_INCREMENT, zeitstempel TIMESTAMP, mentimeterUrl VARCHAR(50), mentimeterKey VARCHAR(10), titel VARCHAR(50), themen VARCHAR(150), urls VARCHAR(200), maxProGruppe INT, dauerNachdenken INT, endeUhrzeitNachdenken DATETIME, dauerVergleichen INT, endeUhrzeitVergleichen DATETIME, dauerKonsens INT, endeUhrzeitKonsens DATETIME, PRIMARY KEY(nummer));", (err) => {
            //if (err) return next(err)         
            console.log("Tabelle 'gruppenpuzzle' erfolgreich angelegt.");
        })
    }else{
        console.log("Tabelle 'gruppenpuzzle' schon vorhanden.");
    }    
})

dbVerbindung.query("SHOW TABLES LIKE 'gpUser';", (rows) => {
    if (!rows){
        dbVerbindung.query("CREATE TABLE IF NOT EXISTS gpUser(gruppe VARCHAR(20), gruppeId INT, block INT, name VARCHAR(50), thema VARCHAR(50), url VARCHAR(50), treffpunkt VARCHAR(50), lfdNummerInDerGruppe INT, zeitstempel TIMESTAMP, PRIMARY KEY(name,gruppe));", (err) => {        
            //if (err) return next(err)         
            console.log("Tabelle 'gpUser' erfolgreich angelegt.");
        })
    }else{
        console.log("Tabelle 'gpUser' schon vorhanden.");
    }    
})

dbVerbindung.query("SHOW TABLES LIKE 'users';", (rows) => {
    if (!rows){
        dbVerbindung.query("CREATE TABLE IF NOT EXISTS users(username VARCHAR(50), password VARCHAR(50), PRIMARY KEY(username));", (err) => {        
            //if (err) return next(err)                 
            console.log("Tabelle 'users' erfolgreich angelegt.");
        })
    }else{
        console.log("Tabelle 'users' schon vorhanden.");
    }    
})

dbVerbindung.query("SHOW TABLES LIKE 'gruppen';", (rows) => {
    if (!rows){
        dbVerbindung.query("CREATE TABLE IF NOT EXISTS gruppen(laufendeNr INT AUTO_INCREMENT, name VARCHAR(50), PRIMARY KEY(laufendeNr));", (err) => {        
            //if (err) return next(err)                 
            console.log("Tabelle 'gruppen' erfolgreich angelegt.");
        })
    }else{
        console.log("Tabelle 'gruppen' schon vorhanden.");
    }    
})

dbVerbindung.query("SELECT * from gpuser;", (err, rows) => {
    if(rows){
        for (i = 0; i < rows.length; i++) {
        
            let gpu = new GpUser()
            gpu.name = rows[i].name
            gpu.gruppe = rows[i].gruppe
            gpu.url = rows[i].url
            gpu.thema = rows[i].thema
            gpu.lfdNummerInDerGruppe = rows[i].lfdNummerInDerGruppe
            gpu.block = rows[i].block
            gpu.treffpunkt = rows[i].treffpunkt
            gpUsers.push(gpu)
        }
    }        
})

dbVerbindung.query("SELECT * from gruppen;", (err, rows) => {
    if(rows){
        for (i = 0; i < rows.length; i++) {
            gruppen.push({name: rows[i].name, laufendeNr: rows[i].laufendeNr})
        }
    }    
})

dbVerbindung.query("SELECT * from gruppenpuzzle;", (err, rows) => {    
    for (i = 0; i < rows.length; i++) {
        nummer = rows[i].nummer
        zeitstempel = rows[i].zeitstempel
        mentimeterUrl = rows[i].mentimeterUrl
        mentimeterKey = rows[i].mentimeterKey    
        titel = rows[i].titel        
        maxProGruppe = rows[i].maxProGruppe
        dauerNachdenken = rows[i].dauerNachdenken
        endeUhrzeitNachdenken = rows[i].endeUhrzeitNachdenken
        dauerVergleichen = rows[i].dauerVergleichen
        endeUhrzeitVergleichen = rows[i].endeUhrzeitVergleichen
        dauerKonsens = rows[i].dauerKonsens
        endeUhrzeitKonsens = rows[i].endeUhrzeitKonsens
        urls = rows[i].urls
        themen = rows[i].themen
    }    
})

app.get('/',(req, res, next) => {                

    // Wenn es keinen Titel gibt oder wenn das letzte Gruppenpuzzle lange her ist ...
        
    if(!titel){     

        // ... wird kein Gruppenpuzzle angeboten.
        

        
        res.render('index.ejs', {                    
            welcome: "Zur Zeit kein aktives Gruppenpuzzle",
            anzeigen: "",
            endeUhrzeitNachdenken: new Date(),
            endeUhrzeitVergleichen: new Date(),
            endeUhrzeitKonsens: new Date(),
            name: "",
            teamName: ""              
        })
    }else{

        let deinemNachbarn = "deinen"

        if(maxProGruppe == 2){
            deinemNachbarn = "deinem"
        }

        res.render('index.ejs', {                    
            welcome: titel,// + "</br></br>läuft seit " + ("0" + (zeitstempel).getHours()).slice(-2) + ":" + ("0" + (zeitstempel).getMinutes()).slice(-2)  + ":" + ("0" + (zeitstempel).getSeconds()).slice(-2) + " Uhr.<br><br> Bilde mit " + deinemNachbarn + " direkten Nachbarn eine <br>" + maxProGruppe + "er Stammgruppe. <br><br>Gebt euch einen gemeinsamen Stammgruppen-Namen.",
            anzeigen: "",
            endeUhrzeitNachdenken: new Date(),
            endeUhrzeitVergleichen: new Date(),
            endeUhrzeitKonsens: new Date(),
            name: "",
            teamName: ""       
        })
    }            
})

app.post('/', (req, res, next) => {    
    
    // Trim und Konvertierung in Großbuchstaben

    let teamName = (req.body.tbxTeamName).trim().toUpperCase().replace(/[^a-z0-9]/gi,'')
    let name = (req.body.tbxName).trim().toUpperCase()

    // Der Name muss gesetzt sein

    //18.6.
    //if(name === "") return next(new Error("Die Name darf nicht leer sein."))    
    
    let teamer = gpUsers.filter((g) => g.name === name)[0]

    // Wenn der Teamname leer ist ...

    /*18.6.
    if(req.body.tbxTeamName === ""){

        // ... und das Mitglied auch noch nicht existiert, ...

        if(!teamer){

            // ... wird abgebrochen

            return next(new Error("Der Teamname darf nicht leer sein."))    
        }
    } */
    
    // Wenn es den Teamer schon gibt ...
    
    if(teamer) {
       
        res.render('index.ejs', {  
            welcome: "Nochmal Hallo, " + teamer.name + " (" + teamer.gruppe + ")",      
            anzeigen: anzeigen(teamer),    
            endeUhrzeitNachdenken: endeUhrzeitNachdenken,
            endeUhrzeitVergleichen: endeUhrzeitVergleichen,
            endeUhrzeitKonsens: endeUhrzeitKonsens,
            name: name,
            teamName: teamer.gruppe       
        })
        return
    }

    // Wenn es den Teamer noch nicht gibt ...

    if(!teamer) {
        
        // ... wird geprüft, ob die Gruppe schon voll ist:

        if((gpUsers.filter((p) => p.gruppe === teamName)).length === maxProGruppe) 
        { 
            return next(new Error("Die Gruppe ist schon mit " + (gpUsers.filter((p) => p.gruppe === teamName)).length + " Teamern belegt.")) 
        }

        // ... wird er angelegt und seine Eigenschaften gesetzt.

        let anzahl = gpUsers.length
        gpUsers.push(new GpUser()) 
        gpUsers[anzahl].name = name                        
        gpUsers[anzahl].gruppe = teamName                    
        
        anzahlMitgliederInDieserGruppe = (gpUsers.filter((p) => p.gruppe === teamName)).length
        
        gpUsers[anzahl].url = urls.split(',')[anzahlMitgliederInDieserGruppe - 1] 
        gpUsers[anzahl].thema = themen.split(',')[anzahlMitgliederInDieserGruppe - 1]
        gpUsers[anzahl].lfdNummerInDerGruppe = anzahlMitgliederInDieserGruppe

        // Wenn es die Gruppe noch nicht gibt, wird sie angelegt.

        if((gruppen.filter((p) => p.name === teamName)).length === 0) {
            let laufendeNr = gruppen.length + 1
            gruppen.push({name: teamName, laufendeNr: laufendeNr})
            gpUsers[anzahl].gruppeId = laufendeNr                
            dbVerbindung.query("INSERT INTO gruppen(laufendeNr, name) VALUES ('" + laufendeNr + "','" + teamName + "';", (err, result) => {                                
            })
        }else{
            gpUsers[anzahl].gruppeId = (gruppen.filter((p) => p.name === teamName)[0].laufendeNr)
        }

        // Bei drei Gruppen und 3 Teamern pro Gruppe beginnt der 2. Block nach der 3. Gruppe

        gpUsers[anzahl].block = Math.ceil(gpUsers[anzahl].gruppeId / maxProGruppe)

        let gr = gruppen.filter((g) => g.name === teamName)[0]

        // Gruppe 1 und Teamer Nr. 1 = Treffpunkt
        // Gruppe 2 und Teamer Nr. 2 = Treffpunkt
        // Gruppe 3 und Teamer Nr. 3 = Treffpunkt
        // Gruppe 4 und Teamer Nr. 1 = Treffpunkt
        // Gruppe 5 und Teamer Nr. 2 = Treffpunkt
        // ...

        if(gpUsers[anzahl].lfdNummerInDerGruppe === (gr.laufendeNr - (maxProGruppe * (gpUsers[anzahl].block - 1)))){                        
            
            if(!gpUsers[anzahl].treffpunkt) gpUsers[anzahl].treffpunkt = name

            // Weitherin wird für alle Teamer ...

            for (i = 0; i < gpUsers.length - 1 ; i++) {

                // ... des selben Blocks und der selben Gruppe, aber mit kleinerer oder größerer Gruppen-ID.

                if(gpUsers[i].block === gpUsers[anzahl].block && gpUsers[i].lfdNummerInDerGruppe === gpUsers[anzahl].lfdNummerInDerGruppe && gpUsers[i].gruppeId != gpUsers[anzahl].gruppeId){
             
                    // ... dieser Treffpunkt gesetzt.    

                    //if(!gpUsers[anzahl].treffpunkt) gpUsers[i].treffpunkt = name

                    gpUsers[i].treffpunkt = name
                    
                    console.log("Der Treffpunkt von " + gpUsers[i].name + " wird auf " + name + " geändert.")

                    dbVerbindung.query("UPDATE gpUser SET treffpunkt = '" + name + "' WHERE name = '" + gpUsers[i].name + "';", (err, result) => {                                    

                    })
                }
            }
        }

        // Für alle ohne Treffpunkt wird versucht den Treffpunkt zu ermitteln

        for (i = 0; i < gpUsers.length - 1; i++) {
            
            // console.log(gpUsers.length + " Name: " + gpUsers[i].name + " TEAMER " + gpUsers[anzahl])
            
            if(gpUsers[anzahl].block == gpUsers[i].block && gpUsers[anzahl].lfdNummerInDerGruppe == gpUsers[i].lfdNummerInDerGruppe && gpUsers[i].treffpunkt){
                
                if(!gpUsers[anzahl].treffpunkt) gpUsers[anzahl].treffpunkt = gpUsers[i].treffpunkt
                // console.log("Treffpunkt des Teamers " + gpUsers[anzahl].name + " " + gpUsers[i].treffpunkt)
            }
        }
        
        dbVerbindung.query("INSERT INTO gpUser(gruppe, gruppeId, block, name, lfdNummerInDerGruppe, thema, url, treffpunkt, zeitstempel) VALUES ('" + gpUsers[anzahl].gruppe + "','" + gpUsers[anzahl].gruppeId + "','" + gpUsers[anzahl].block + "','" + gpUsers[anzahl].name + "','" + gpUsers[anzahl].lfdNummerInDerGruppe + "','" + gpUsers[anzahl].thema + "','" + gpUsers[anzahl].url + "','" + gpUsers[anzahl].treffpunkt + "' , NOW());", (err, result) => {                
            if (err) return next(err)                

            res.render('index.ejs', {  
                welcome: "Hallo, " + gpUsers[anzahl].name + " (" + gpUsers[anzahl].gruppe + ")",      
                anzeigen: anzeigen(gpUsers[anzahl]),    
                endeUhrzeitNachdenken: endeUhrzeitNachdenken,
                endeUhrzeitVergleichen: endeUhrzeitVergleichen,
                endeUhrzeitKonsens: endeUhrzeitKonsens,
                name: name,
                teamName: teamName              
            })        
        })    
    }    
})

app.use((err, req, res, next) => {    
    console.log(err.stack)
    res.render('error.ejs', {        
        error:["F E H L E R", err.message, "Falls Du nicht automatisch weitergeleitet wirst, dann ...", "Seite neu laden, um fortzufahren."]
    }) 
})

app.get('/admin', (req, res,next) => {    

    // Wenn die Admin-Seite aufgerufen wird, werden die Themen aus der DB in das Programm geladen.

    dbVerbindung.query("SELECT * from gruppenpuzzle;", (err, rows) => {    
        for (i = 0; i < rows.length; i++) {
            nummer = rows[i].nummer
            zeitstempel = rows[i].zeitstempel
            mentimeterUrl = rows[i].mentimeterUrl
            mentimeterKey = rows[i].mentimeterKey    
            titel = rows[i].titel        
            maxProGruppe = rows[i].maxProGruppe
            dauerNachdenken = rows[i].dauerNachdenken
            endeUhrzeitNachdenken = rows[i].endeUhrzeitNachdenken
            dauerVergleichen = rows[i].dauerVergleichen
            endeUhrzeitVergleichen = rows[i].endeUhrzeitVergleichen
            dauerKonsens = rows[i].dauerKonsens
            endeUhrzeitKonsens = rows[i].endeUhrzeitKonsens
            urls = rows[i].urls
            themen = rows[i].themen
        }    
    })

    dbVerbindung.query("SELECT *, (TIMESTAMPDIFF(SECOND, endeUhrzeitKonsens, NOW())) AS placematVorbeiSeitSekunden from gruppenpuzzle;", (err, rows) => {
        if (err) return next(err)         

        // Wenn es kein Gruppenpuzzle gibt:

        if(!rows.length){      
            
            res.render('admin.ejs', {        
                anzeigen: [],
                titel: "Mein Gruppenpuzzle",
                themen: "",
                urls: "",
                maxProGruppe: 3,
                dauerNachdenken: 5,
                mentimeterUrl: "",
                mentimeterKey: "",
                dauerVergleichen: 5,
                dauerKonsens: 5,
                absenden: "absenden",
                placematUser: null
            })
        }else{                       

            endeUhrzeitNachdenken = rows[0].endeUhrzeitNachdenken
            endeUhrzeitVergleichen = rows[0].endeUhrzeitVergleichen
            endeUhrzeitKonsens = rows[0].endeUhrzeitKonsens
            
            dbVerbindung.query("SELECT * from gpUser ORDER BY gruppeId,lfdNummerInDerGruppe;", (err, result) => {            
                if (err) return next(err)
                
                let anzeigen = []            
                let button = ""
                // Falls das Placemat bereits abgelaufen ist:

                if(rows[0].placematVorbeiSeitSekunden > 0){
                    anzeigen.push("Zurzeit kein aktives Gruppenpuzzle.")                    
                    button = "Neu anlegen"
                }else{                    
                    button = "Placemat aktiv"
                }
                anzeigen.push("<tr><td> Wann?</td><td>Was?</td></tr>") 
                anzeigen.push("<tr><td> jetzt</td><td>EXPERTENGRUPPE</td></tr>") 
                //anzeigen.push("<tr><td> " + ("0" + endeUhrzeitNachdenken.getHours()).slice(-2) +":" + ("0" + endeUhrzeitNachdenken.getMinutes()).slice(-2) + " Uhr</td><td> STUMMES VERGLEICHEN</td></tr>")                 
                anzeigen.push("<tr><td> " + ("0" + endeUhrzeitVergleichen.getHours()).slice(-2) +":" + ("0" + endeUhrzeitVergleichen.getMinutes()).slice(-2) + " Uhr</td><td>STAMMGRUPPE</td></tr>")                 
                anzeigen.push("<tr><td> " + ("0" + endeUhrzeitKonsens.getHours()).slice(-2) +":" + ("0" + endeUhrzeitKonsens.getMinutes()).slice(-2) + " Uhr</td><td>PÄSENTATION</td></tr>")
    
                titel = rows[0].titel

                res.render('admin.ejs', {
                    anzeigen: anzeigen,
                    titel: rows[0].titel,
                    themen: rows[0].themen,
                    urls: rows[0].urls,
                    maxProGruppe: rows[0]. maxProGruppe,
                    dauerNachdenken: rows[0].dauerNachdenken,
                    dauerVergleichen: rows[0].dauerVergleichen,
                    dauerKonsens: rows[0].dauerKonsens,
                    mentimeterUrl: rows[0].mentimeterUrl,
                    mentimeterKey: rows[0].mentimeterKey,
                    absenden: button,
                    placematUser: result
                })
            })
        }        
    })                
})

app.post('/admin', (req, res, next) => {    
        
    console.log("Timestamp: " + zeitstempel)
    
    dbVerbindung.query("SELECT COUNT(*) AS anzahl FROM users WHERE (username = '" + req.body.tbxUsername + "') AND (password = '" + req.body.tbxPassword + "');", (err,rows) => {                     
        if (err) return next(err)

        if (rows[0].anzahl === 1){

            gpUsers = []
            gruppen = []

            dbVerbindung.query("DELETE from gruppenpuzzle;", (err) => {        
                if (err) return next(err)
            })
        
            dbVerbindung.query("DELETE from gpUser;", (err) => {
                if (err) return next(err)
            })

            dbVerbindung.query("DELETE from gruppen;", (err) => {
                if (err) return next(err)
            })

            console.log("Dauer Nachdenken:" + req.body.tbxNachdenken)

            themen = req.body.themen
            titel = req.body.titel            
            mentimeterUrl = req.body.mentimeterUrl
            mentimeterKey = req.body.mentimeterKey
            maxProGruppe = req.body.maxProGruppe
            dauerNachdenken = req.body.dauerNachdenken
            urls = req.body.urls

            dbVerbindung.query("INSERT INTO gruppenpuzzle(titel, mentimeterUrl, mentimeterKey, themen, urls, zeitstempel, maxProGruppe, dauerNachdenken, endeUhrzeitNachdenken, dauerVergleichen, endeUhrzeitVergleichen, dauerKonsens, endeUhrzeitKonsens) VALUES ('" + req.body.tbxTitel + "','" + req.body.tbxMentimeterUrl + "','"  + req.body.tbxMentimeterKey + "','" + req.body.tbxThemen + "','" + req.body.tbxUrls + "', now(), '" + req.body.tbxMaxProGruppe + "','" + req.body.tbxNachdenken + "', ADDTIME(now(), '0:" + req.body.tbxNachdenken + ":0'),'" + 0 + "', ADDTIME(now(), '0:" + (0 + parseInt(req.body.tbxNachdenken)) + ":0'),'" + req.body.tbxKonsens + "', ADDTIME(now(), '0:" + (0 + parseInt(req.body.tbxNachdenken) + parseInt(req.body.tbxKonsens)) + ":0'));", (err) => {                                     
                if (err) return next(err)

                dbVerbindung.query("SELECT zeitstempel, endeUhrzeitNachdenken, endeUhrzeitVergleichen FROM gruppenpuzzle;", (err, rows) => {                                     
                    if (err) return next(err)

                    zeitstempel = rows[0].zeitstempel
                    endeUhrzeitNachdenken = rows[0].endeUhrzeitNachdenken
                    endeUhrzeitVergleichen = rows[0].endeUhrzeitVergleichen
                })
        
                res.render('admin.ejs', {            
                    anzeigen: [],
                    titel: req.body.tbxTitel,
                    themen: req.body.tbxThemen,
                    urls: req.body.urls,
                    maxProGruppe: req.body.tbxMaxProGruppe,
                    dauerNachdenken: req.body.tbxNachdenken,
                    dauerVergleichen: req.body.tbxVergleichen,
                    dauerKonsens: req.body.tbxKonsens,
                    mentimeterUrl: req.body.tbxMentimeterUrl,
                    mentimeterKey: req.body.tbxMentimeterKey,
                    absenden: "Gruppenpuzzle aktiv!",
                    placematUser: null
                })
            })    
        }else{            
            return next(new Error("Du hast keine Berechtigung dies zu tun!"))
        }
    })
})

function anzeigen(gpUser){
    
    console.log(gpUser.treffpunkt)

    let wer1 = ""
    let wer2 = ""
    let wer2url = ""

    for (i = 0; i < gpUsers.length; i++) {
        
        // Mitglieder derselben Expertengruppe sind alle mit der selben Nummer in der eigenen Gruppe ...

        if(gpUsers[i].lfdNummerInDerGruppe === gpUser.lfdNummerInDerGruppe && gpUsers[i].block === gpUser.block){

            wer1 = wer1 + gpUsers[i].name + ", "
            if(gpUsers[i].lfdNummerInDerGruppe === gpUsers[i].laufendeNr){
                if(!gpUsers[anzahl].treffpunkt) gpUser.treffpunkt = gpUsers[i].name
            }
        }
        if(gpUsers[i].gruppe === gpUser.gruppe){
            wer2 = wer2 + gpUsers[i].name + ", "            
            wer2url = wer2url + "</br>" + gpUsers[i].name + ": <a target='_blank' href='" + gpUsers[i].url + "'> " + gpUsers[i].thema + "</a> "
        }
    }

    wer1 = wer1.replace(/,\s*$/, "");
    wer2 = wer2.replace(/,\s*$/, "");
    wer2url = wer2url.replace(/,\s*$/, "");

    let treffpunkt = gpUser.treffpunkt
    if(!treffpunkt){
        treffpunkt = "Bitte in 30 Sekunden Seite mit F5 neu laden!"
    }

    //console.log(endeUhrzeitNachdenken + "---" + endeUhrzeitKonsens)

    let anzeigen = [ "<tr><th></th><th>Wann?</th><th>Wo?</th><th>Wer?</th><th>Was?</th></tr>"]                    
    anzeigen.push("<tr><td><h4>1.</h4></td><td>jetzt</td><td>"+ treffpunkt + "</td><td>" + wer1 + "</td><td><p>Dein Expertenthema:<br> <a target='_blank' href='" + gpUser.url + "'> " + gpUser.thema + "</a>")     
    anzeigen.push("<tr><td><h4>2.</h4></td><td>11:40 Uhr</td><td>" + gpUser.gruppe + "</td><td>" + wer2 + "</td><td>Berichtet Euch gegenseitig und programmiert den Quelltext aus!</td></tr>")         
    anzeigen.push("<tr><td><h4>3.</h4></td><td>11:50 Uhr</td><td>Plenum</td><td>Plenum</td><td>Eine Gruppe berichtet von der Umsetzung.</td></tr>")    
    
    //anzeigen.push("<tr><td><h4>2.</h4></td><td>" + ("0" + endeUhrzeitNachdenken.getHours()).slice(-2) +":" + ("0" + endeUhrzeitNachdenken.getMinutes()).slice(-2) + " Uhr</td><td>" + gpUser.gruppe + "</td><td>" + wer2 + "</td><td>Berichtet Euch gegenseitig und programmiert den Quelltext aus!</td></tr>")         
    
    //anzeigen.push("<tr><td><h4>3.</h4></td><td>" + ("0" + endeUhrzeitKonsens.getHours()).slice(-2) +":" + ("0" + endeUhrzeitKonsens.getMinutes()).slice(-2) + " Uhr</td><td>Plenum</td><td>Plenum</td><td>Eine Gruppe berichtet von der Umsetzung.</td></tr>")    
    
    if(mentimeterUrl){
        anzeigen.push("<tr><td><h4></h4></td><td></td><td></td><td></td><td><a target='_blank' href='" + mentimeterUrl + "'>Feedback</a> (" + mentimeterKey + ") </td></tr>")   
    }

    if(mentimeterUrl){
        anzeigen.push("<tr><td><h4></h4></td><td></td><td></td><td></td><td><a target='_blank' href='/wuerfel'>Würfeln</a></td></tr>")   
    }
    
    return anzeigen
}


app.get('/wuerfel',(req, res, next) => {                

    let gruppenWuerfel = ""

    dbVerbindung.query("SELECT DISTINCT gruppe from gpUser ORDER BY gruppe;", (err, rows) => {

        gruppenWuerfel = "<table>"

        for (i = 0; i < rows.length; i++) {                        
            gruppenWuerfel += '<tr><td>' + rows[i].gruppe + '</td></tr>'            
        }    

        gruppenWuerfel = gruppenWuerfel + "</table>"

        res.render('wuerfel.ejs', {                    
            welcome: "Stammgruppe auswürfeln",
            gruppenWuerfel: gruppenWuerfel,
            random: ""            
        })   
    })
})

app.post('/wuerfel', (req, res, next) => {    

    let gruppenWuerfel = []

    dbVerbindung.query("SELECT DISTINCT gruppe from gpUser ORDER BY gruppe;", (err, rows) => {

        let min = 0
        let max = rows.length - 1 
        let x = Math.floor(Math.random()*(max-min+1)+min)

        gruppenWuerfel = "<table>"
        
        dbVerbindung.query("SELECT * from gpUser where gruppe = '" + rows[x].gruppe + "';", (err, rowss) => {
        
            // Wenn in einer Gruppe genau zwei Teilnehmer sind, dann werden die Themen getauscht.

            let meldung = ""

            for (i = 0; i < rows.length; i++) {                        
                if(x === i){
                    if(maxProGruppe == 2){
                        meldung = "<ol><li>Zuerst präsentiert " + rowss[1].name + " das Thema " + rowss[0].thema + ".<li>Danach präsentiert " + rowss[0].name + " das Thema " + rowss[1].thema + ".</ol>"                                    
                    }
                    gruppenWuerfel += '<tr><td>' + rows[i].gruppe + '</td><td>' + meldung + '</td></tr>'            
                }else{
                    gruppenWuerfel += '<tr><td>' + rows[i].gruppe + '</td><td></td></tr>'
                }
            }    

            gruppenWuerfel = gruppenWuerfel + "</table>"

            res.render('wuerfel', {                    
                welcome: "Juhu!",
                gruppenWuerfel: gruppenWuerfel,
                random: ""            
            })   
        })
    })
})
